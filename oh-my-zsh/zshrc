# Path to your oh-my-zsh installation.
ZSH=${OHMYZSH_PATH:=/usr/share/oh-my-zsh}

# Set name of the theme to load.
ZSH_THEME="powerline"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd/mm/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=$HOME/.config/dotfiles/oh-my-zsh

# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(history-substring-search rust poetry asdf)

# User configuration
export PATH=$HOME/bin:/usr/local/bin:$PATH
export MANPATH="/usr/local/man:/usr/share/man:$MANPATH"

ZSH_CACHE_DIR=$HOME/.oh-my-zsh-cache
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

# Set autorehash
zstyle ':completion:*' rehash true

# Set extended globbing
setopt extended_glob

# MicroK8s completions - Aliases must be created, p.e.:
# `sudo snap alias microk8s.kubectl mkubectl`
if which mkubectl > /dev/null 2>&1; then
  source <(mkubectl completion zsh 2> /dev/null | sed "s/kubectl/mkubectl/g")
fi
if which mhelm > /dev/null 2>&1; then
  source <(mhelm completion zsh 2> /dev/null | sed "s/helm/mhelm/g")
fi
if which mlinkerd > /dev/null 2>&1; then
  source <(mlinkerd completion zsh 2> /dev/null | sed "s/linkerd/mlinkerd/g")
fi

# OpenShift completion
if [ $commands[oc] ]; then
  source <(oc completion zsh)
  compdef _oc oc
fi
