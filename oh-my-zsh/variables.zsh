# Unset Variables
#unset SSH_ASKPASS

# No blank line in 'powerline' zsh theme
POWERLINE_NO_BLANK_LINE="no"

# Editor
export EDITOR=nvim
export SVN_EDITOR=nvim
export DIFFPROG="nvim -d $1"

# GCC config
export GCC_COLORS=1

# grep config
export GREP_COLORS='ms=01;31:mc=01;31:sl=:cx=:fn=34:ln=36:bn=32:se=36'

# History Substring Search
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='fg=blue,bold'
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='fg=red,bold'
