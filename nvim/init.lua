-- General configuration
require("settings")
-- Lazy bootstraping and config
require("lazy-boot")
-- Global mappings and autocmds
require("functions")
require("autocmds")
require("mappings")

