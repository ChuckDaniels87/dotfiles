-- NeoVim global settings

-- General options
vim.o.hidden = true
vim.o.undofile = false
vim.o.joinspaces = false
vim.o.backupcopy = "yes"
vim.o.inccommand = "split"
vim.o.scrolloff = 4

-- Force Python paths
vim.g.python_host_prog = "/usr/bin/python2"
vim.g.python3_host_prog = "/usr/bin/python3"

-- Leader key
vim.g.mapleader = ","

-- Tab and stop options
vim.o.expandtab = true
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.bo.textwidth = 80
vim.o.list = true
vim.opt.listchars = {
    tab = "» ",
    trail = "·",
    nbsp = "␣",
}

-- Completion
vim.o.updatetime = 300
vim.o.cmdheight = 2
vim.o.signcolumn = "yes"

-- Folding
vim.o.foldlevel = 20
vim.o.foldlevelstart = 20

-- Spell check
vim.o.spelllang = "en_us"

-- Colors
vim.o.termguicolors = true

-- Color Column
vim.wo.colorcolumn = "0"

-- Diagnostics
vim.diagnostic.config({
    float = {
        source = true,
    },
    virtual_text = false,
})

-- Signs
local signs = {
    { name = "DiagnosticSignError", text = "✗" },
    { name = "DiagnosticSignWarn", text = "●", },
    { name = "DiagnosticSignInfo", text = "ℹ" },
    { name = "DiagnosticSignHint", text = "." },
}
for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
end
