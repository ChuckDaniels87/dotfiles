-- Utility functions module
functions = {}

-- Toggle spell checking
function functions.toggle_spell()
    vim.wo.spell = not vim.wo.spell
end

-- Toggle color column
function functions.toggle_color_column()
    if vim.wo.colorcolumn ~= "0" then
        vim.wo.colorcolumn = "0"
    else
        vim.wo.colorcolumn = tostring(vim.o.tw + 1)
    end
end

-- Toggle cursor line
function functions.toggle_cursor_line()
    vim.wo.cursorline = not vim.wo.cursorline
end

-- Toggle line number
function functions.toggle_line_number()
    vim.wo.number = not vim.wo.number
end

-- Replace term codes for the given string
-- str Input string
function functions.tc(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

-- Check if the previous character is a space
function functions.check_back_space()
    local col = vim.api.nvim_win_get_cursor(0)[2]
    return (col == 0 or vim.api.nvim_get_current_line():sub(col, col):match('%s')) and true
end

-- Function to check if a floating dialog exists and if not
-- then check for diagnostics under the cursor
function functions.open_diagnostics()
    for _, winid in pairs(vim.api.nvim_tabpage_list_wins(0)) do
        if vim.api.nvim_win_get_config(winid).zindex then
            return
        end
    end
    -- THIS IS FOR BUILTIN LSP
    vim.diagnostic.open_float(0, {
        scope = "line",
        focusable = false,
        close_events = {
            "CursorMoved",
            "CursorMovedI",
            "BufHidden",
            "InsertCharPre",
            "WinLeave",
        },
    })
end

return functions
