return {
    {
        "nvim-tree/nvim-tree.lua",
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
        opts = {
            disable_netrw = true
        },
        config = function(_, opts)
            local nvimtree = require("nvim-tree")
            nvimtree.setup(opts)

            local api = require("nvim-tree.api")
            vim.keymap.set("n", "<F2>", function() api.tree.toggle({ find_file = true }) end)
        end
    },
    {
        "antosha417/nvim-lsp-file-operations",
        dependencies = {
            "nvim-tree/nvim-tree.lua",
        },
        config = function()
            require("lsp-file-operations").setup()
        end,
    },
}
