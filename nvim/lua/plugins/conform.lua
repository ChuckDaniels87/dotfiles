return {
    "stevearc/conform.nvim",
    config = function()
        local conform = require("conform")
        conform.setup({
            formatters_by_ft = {
                python = { "ruff_format" },
                yaml = { "prettier" },
                json = { "prettier" },
                css = { "prettier" },
                less = { "prettier" },
                javascript = { "prettier" },
                javascriptreact = { "prettier" },
                typescript = { "prettier" },
                typescriptreact = { "prettier" },
                gdscript = { 'gdformat' },
            },
        })

        local function format_code()
            conform.format({ async = true, lsp_fallback = true })
        end

        vim.keymap.set('n', '<F8>', format_code, { noremap = true, silent = true })
    end
}
