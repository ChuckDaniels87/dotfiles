return {
    {
        "williamboman/mason.nvim",
        config = true,
    },
    {
        "WhoIsSethDaniel/mason-tool-installer.nvim",
        opts = {
            ensure_installed = {
                -- LSP Servers
                "cssls",
                "gopls",
                "jdtls",
                "lua_ls",
                "pyright",
                "ruff",
                "rust_analyzer",
                "vtsls",
                -- Formatters
                "prettier",
                "gdtoolkit",
                -- Linters
                "staticcheck",
                "eslint-lsp",
                "biome",
            }
        },
    },
    {
        "williamboman/mason-lspconfig.nvim",
        dependencies = {
            "neovim/nvim-lspconfig",
            "saghen/blink.cmp",
        },
        config = function()
            local blink     = require("blink.cmp")
            local lspconfig = require("lspconfig");

            local borders   = {
                ['textDocument/hover'] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" }),
                ['textDocument/signatureHelp'] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" }),
            }

            -- Other LSP used outside Mason
            lspconfig.gdscript.setup({})

            -- Mason configured LSP
            require("mason-lspconfig").setup_handlers({
                -- Default config
                function(server_name)
                    local capabilities = blink.get_lsp_capabilities()
                    lspconfig[server_name].setup({
                        handlers = borders,
                        capabilities = capabilities
                    })
                end,
                -- Dedicated handlers
                ["lua_ls"] = function()
                    local capabilities = blink.get_lsp_capabilities()
                    lspconfig.lua_ls.setup({
                        handlers = borders,
                        capabilities = capabilities,
                        settings = {
                            Lua = {
                                diagnostics = {
                                    globals = { "vim" }
                                }
                            }
                        }
                    })
                end,
                ["gopls"] = function()
                    local capabilities = blink.get_lsp_capabilities()
                    lspconfig.gopls.setup({
                        handlers = borders,
                        capabilities = capabilities,
                        settings = {
                            gopls = {
                                analyses = {
                                    unusedparams = true
                                },
                                usePlaceholders = true,
                                staticcheck = true
                            }
                        }
                    })
                end,
            })
        end

    },
}
