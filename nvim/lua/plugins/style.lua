return {
    -- Theme
    {
        "rebelot/kanagawa.nvim",
        config = function()
            require('kanagawa').setup({
                colors = {
                    theme = {
                        all = {
                            ui = {
                                bg_gutter = "none"
                            }
                        }
                    }
                },
                overrides = function(colors)
                    local theme = colors.theme

                    local make_diagnostic_color = function(color)
                        local c = require("kanagawa.lib.color")
                        return { fg = color, bg = c(color):blend(theme.ui.bg, 0.95):to_hex() }
                    end

                    return {
                        -- Diagnostics
                        DiagnosticVirtualTextHint  = make_diagnostic_color(theme.diag.hint),
                        DiagnosticVirtualTextInfo  = make_diagnostic_color(theme.diag.info),
                        DiagnosticVirtualTextWarn  = make_diagnostic_color(theme.diag.warning),
                        DiagnosticVirtualTextError = make_diagnostic_color(theme.diag.error),
                        -- Floating menu
                        NormalFloat                = { bg = theme.ui.bg },
                        FloatTitle                 = { fg = theme.ui.fg_dim, bg = theme.ui.bg },
                        FloatBorder                = { fg = theme.ui.bg_p2, bg = theme.ui.bg },
                        -- Completion menu
                        Pmenu                      = { fg = theme.ui.shade0, bg = theme.ui.bg_p1 },
                        PmenuSel                   = { fg = "NONE", bg = theme.ui.bg_p2 },
                        PmenuSbar                  = { bg = theme.ui.bg_m1 },
                        PmenuThumb                 = { bg = theme.ui.bg_p2 },
                        -- Blink custom colors
                        BlinkCmpDocBorder          = { fg = theme.ui.bg_p2, bg = theme.ui.bg },
                    }
                end
            })
            vim.cmd.colorscheme("kanagawa")
        end
    },
    -- Syntax
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require('nvim-treesitter.configs').setup({
                highlight = { enable = true },
                indent = { enable = false },
                fold = { enable = true }
            })

            vim.o.foldmethod = "expr"
            vim.o.foldexpr = "nvim_treesitter#foldexpr()"
        end
    },
    "ntpeters/vim-better-whitespace",
    -- Icons
    "ryanoasis/vim-devicons",
    {
        "kyazdani42/nvim-web-devicons",
        opts = { default = true }
    },
    -- UI
    {
        "stevearc/dressing.nvim",
        opts = {}
    },
    -- Colors
    {
        "brenoprata10/nvim-highlight-colors",
        config = function()
            require('nvim-highlight-colors').setup({
                render = 'virtual',
                virtual_symbol = '●',
                virtual_symbol_prefix = '',
                virtual_symbol_suffix = '',
            })
        end
    },
}
