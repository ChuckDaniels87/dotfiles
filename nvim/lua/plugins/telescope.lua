return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        "nvim-telescope/telescope-ui-select.nvim",
    },
    config = function()
        local telescope = require("telescope")
        telescope.setup({
            defaults = {
                sorting_strategy = "ascending",
                mappings = {
                    i = {
                        ["<esc>"] = require("telescope.actions").close
                    },
                },
                layout_strategy = "flex",
                layout_config = {
                    prompt_position = "top",
                    flex = {
                        flip_columns = 160,
                    }
                }
            },
            extensions = {
                ["ui-select"] = {
                    require("telescope.themes").get_dropdown({})
                }
            }
        })

        require("telescope").load_extension("ui-select")

        local builtin = require("telescope.builtin")
        local opts = { silent = true }

        local function all_pickers()
            builtin.builtin({ include_extensions = true })
        end

        vim.keymap.set("n", "<F5>", all_pickers, opts)
        vim.keymap.set("n", "<C-F5>", builtin.resume, opts)
        vim.keymap.set("n", "<F6>", builtin.diagnostics, opts)
        vim.keymap.set("n", "gr", builtin.lsp_references, opts)
    end
}
