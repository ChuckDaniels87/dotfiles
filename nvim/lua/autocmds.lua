-- NeoVim auto-command configuration

-- Custom textwidth
vim.cmd([[
    autocmd FileType * setlocal tw=80
    autocmd FileType gitcommit setlocal tw=72 sw=2
    autocmd FileType go setlocal tw=120
    autocmd FileType java setlocal tw=120
    autocmd FileType javascript setlocal tw=120
    autocmd FileType Jenkinsfile setlocal tw=120
    autocmd FileType helm setlocal tw=120
    autocmd FileType python setlocal tw=88
    autocmd FileType rust setlocal tw=100
    autocmd FileType svn setlocal tw=72 sw=2
    autocmd FileType typescript setlocal tw=120
]])

-- Show diagnostics under the cursor when holding position
vim.api.nvim_create_augroup("lsp_diagnostics_hold", { clear = true })
vim.api.nvim_create_autocmd({ "CursorHold" }, {
    pattern = "*",
    command = "lua functions.open_diagnostics()",
    group = "lsp_diagnostics_hold",
})
